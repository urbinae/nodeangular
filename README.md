# nodeangular

Nodejs Angular Sequelize SQLServer

## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/urbinae/nodeangular.git
git branch -M main
git push -uf origin main
```

## Ejecutar el cliente
ng serve

## Ejecutar Servidor de nodejs
npm run dev

## Configuraciones en los archivos
- testClient/proxy.conf.json  
```
{  
    "/api/users/*": {  
        "target": "http://  localhost:3000",  
        "secure": "false",  
        "logLevel": "debug"  
    }
}
```

- testClient/package.json
```
{
    ...
    "scryps":
        "start": "ng serve --proxy-conf proxy.conf.json",
...
}
```

- testServer/config.js

```
module.exports = {
    database: {
        username: "sa",
        password: "Y%&opsa2d",
        database: "testgsoxware",
        host: "localhost"
    },
    auth:{
        expidesIn: 3600,
        secretKey: 'secretkey123456'
    }
}
```

- testClient/src/SERVICES/auth.services.js

```
AUTHSERVER: string = "http://localhost:3000"
```
