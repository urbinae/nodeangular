const User = require('../database/models/User')
const UserAuth = require('../database/models/Auth')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const conf = require('../config')

const usersGet = async (req, res) => {

    const response = await User.findAll()
        .then(function (data) {
            const res = { success: true, data: data }
            return res;
        })
        .catch(error => {
            const res = { success: false, error: error }
            return res;
        })
    res.json(response.data);
};

const usersGetByState = async (req, res) => {

    const state = req.params.state
    const response = await User.findAll({
        where: {
            state: state,
        }
    })
        .then(function (data) {
            const res = { success: true, data: data }
            return res;
        })
        .catch(error => {
            const res = { success: false, error: error }
            return res;
        })
    res.json(response.data);
};

const userGet = async (req, res) => {

    const response = await User.findByPk(req.params.id)
        .then(function (data) {
            const res = { success: true, data: data }
            return res;
        })
        .catch(error => {
            const res = { success: false, error: error }
            return res;
        })
    res.json(response.data);
};

const userPost = async (req, res) => {
    try {
        const { name, lastname, createon, state } = req.body;
        const response = await User.create({
            name,
            lastname,
            createon: new Date(),
            state
        })
            .then(function (data) {
                const res = { success: true, data: data, message: "Registro creado con exito" }
                return res;
            })
            .catch(error => {
                const res = { success: false, error: error }
                return res;
            })
        res.json(response);

    } catch (e) {
        // console.log(e);
    }
}

const userPut = async (req, res) => {
    try {

        const id = req.params.id;
        const { name, lastname, createon, state } = req.body;
        const response = await User.update({
            name,
            lastname,
            createon: new Date(),
            state
        }, {
            where: { id: id }
        })
            .then(function (data) {
                if (!data[0])
                    return { success: false, data: data, message: "Registro no existe" }
                return { success: true, data: data, message: "Registro actualizado con exito" }
            })
            .catch(error => {
                const res = { success: false, error: error }
                return res;
            })
        res.json(response);

    } catch (e) {
        // console.log(e);
    }
}

const userDelete = async (req, res) => {
    try {
        const { id } = req.params;
        const response = await User.destroy({
            where: { id: id }
        })
            .then(function (data) {
                const res = { success: true, data: data, message: "Registro eliminado con exito" }
                return res;
            })
            .catch(error => {
                const res = { success: false, error: error }
                return res;
            })
        res.json(response);

    } catch (e) {
        // console.log(e);
    }
}

const UserLogin = async (req, res) => {
    try {
        const userData = {
            email: req.body.email,
            password: req.body.password
        }

        const userauth = await UserAuth.findOne({
            where: {
                email: userData.email
            }
        })
            .then(function (user) {

                return user;
            })
            .catch(error => {
                return error;
            })
        if (userauth) {
            // const dataUser = {}

            const resultPassword = bcrypt.compareSync(userData.password, userauth.password);
            if (resultPassword) {
                const expiresIn = conf.auth.expidesIn;
                const SECRET_KEY = conf.auth.secretKey
                const accessToken = jwt.sign({ id: userauth.id }, SECRET_KEY, { expiresIn: expiresIn });

                const dataUser = {
                    username: userauth.username,
                    email: userauth.email,
                    accessToken: accessToken,
                    expiresIn: expiresIn
                }
                res.json({ success: true, dataUser, message: "Logeado con éxito" })
                // res.send({ data });
            } else {
                res.json({ success: false, dataUser: null, message: 'Contraseña inválida' })
                // res.status(409).send({ message: 'Contraseña inválida' });
            }
        }
        else {
            // res.status(409).send({ message: 'Dirección de correo no registrada' });
            res.json({ success: false, dataUser: null, message: 'Dirección de correo no registrada' })
        }
        // res.status(409).send({ message: 'Algo anda mal' });
        res.json({ success: false, dataUser: null, message: 'Algo anda mal' })

    } catch (e) {
        //console.log(e);
    }
}

const UserRegister = async (req, res) => {
    try {
        const newUser = {
            username: req.body.username,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password)
        }
        // return res.json({newUser})
        const userauth = await UserAuth.findOne({
            where: {
                email: newUser.email
            }
        })
            .then(function (user) {

                return user;
            })
            .catch(error => {
                return error;
            })
        // return res.json({userauth})
        if (userauth)
            res.json({ success: false, dataUser: null, message: 'La dirección de correo ya existe' })

        const response = await UserAuth.create({
            username: newUser.username,
            email: newUser.email,
            password: newUser.password
        })
            .then(function (user) {
                const expiresIn = conf.auth.expidesIn;
                const SECRET_KEY = conf.auth.secretKey

                // const expiresIn = 24 * 60 * 60;
                const accessToken = jwt.sign({ id: user.id },
                    SECRET_KEY, {
                    expiresIn: expiresIn
                });
                const dataUser = {
                    name: user.name,
                    email: user.email,
                    accessToken: accessToken,
                    expiresIn: expiresIn
                }
                // response 
                const res = { success: true, data: dataUser, message: "Registro creado con exito" }
                // res.send({ dataUser });
                //res.json(response);
                return res
            })
            .catch(error => {
                const res = { success: false, error: error }
                return res;
            })
        res.json(response);

    } catch (e) {
        // console.log(e);
    }
}

module.exports = {
    usersGet,
    userGet,
    userPost,
    userPut,
    userDelete,
    usersGetByState,
    UserLogin,
    UserRegister
}