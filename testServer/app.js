const { urlencoded } = require('express')
const express = require('express')
const app = express()
const port = process.env.PORT || 3000

const bodyParser = require('body-parser')
const cors = require('cors');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(cors());
//Middleware para poder rellenar el req.body
app.use(express.json())
app.use(urlencoded({ extended: false }))

app.use('/api/users', require('./routes/users'))

app.listen(port, () => {
    console.log(`Escuchando por el puerto ${port}`)
})