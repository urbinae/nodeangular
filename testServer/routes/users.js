const express = require('express')
const { userGet, userPost, userPut, userDelete, usersGet, usersGetByState, UserLogin, UserRegister } = require('../controllers/userContrloller')
const router = express.Router()
const { validarCampos } = require('../middlewares/validator')
const { check } = require('express-validator')

//Listar todos
router.get('/', usersGet)

//Listar por state
router.get('/bystate/:state', usersGetByState)

//Create api/users/create
router.post('/create', [
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('lastname', 'El apellido es obligatorio').not().isEmpty(),
    validarCampos
], userPost)

//Read api/users/:id
router.get('/:id', userGet)

//Update api/users/update/:id
router.put('/update/:id', [
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('lastname', 'El apellido es obligatorio').not().isEmpty(),
    validarCampos
], userPut)

//Delete api/users/:id
router.delete('/:id', userDelete)

//login
router.post('/login', UserLogin);

//register
router.post('/register', UserRegister)

module.exports = router