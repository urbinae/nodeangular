const Sequelize = require('sequelize');

const conexion = require('../db');

const User = conexion.define('users', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: Sequelize.STRING,
    lastname: Sequelize.STRING,
    createon: Sequelize.DATE,
    state: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    }
});

module.exports = User;