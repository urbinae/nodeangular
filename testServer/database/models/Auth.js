const Sequelize = require('sequelize');

const conexion = require('../db');

const UserAuth = conexion.define('auths', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
});

module.exports = UserAuth;