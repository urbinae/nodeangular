import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './componentes/create/create.component';
import { DashboardComponent } from './componentes/dashboard/dashboard.component';
import { UpdateComponent } from './componentes/update/update.component';

const routes: Routes = [
  { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
  { path: 'create', component: CreateComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'update/:id', component: UpdateComponent },
  { path: 'auth', loadChildren: () => import("./auth/auth.module").then(m => m.AuthModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
