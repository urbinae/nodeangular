import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/SERVICES/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  noSuccess: Boolean = false
  message: string = ''

  constructor(private authService: AuthService, private router: Router) {
    // if (authService.noSuccess) {
    //   this.noSuccess = authService.noSuccess
    //   this.message = authService.message
    // }
  }

  ngOnInit(): void {
  }

  onRegister(form: { value: any; }): void {
    // this.authService.checkToken();
    this.authService.register(form.value).subscribe(res => {
      console.log(res)
      console.log(this.noSuccess)
      console.log(this.message)
      if (!res.success) {
        this.noSuccess = true
        this.message = res.message
      }
      if (res.success) {
        this.router.navigateByUrl('/auth/login');
      }
    });
  }
}
