import { Component, OnInit, Input } from '@angular/core';

declare const getElementById: any;

@Component({
  selector: 'app-modaldelete',
  templateUrl: './modaldelete.component.html',
  styleUrls: ['./modaldelete.component.css']
})
export class ModaldeleteComponent implements OnInit {

  @Input()
  userid: number = 0

  @Input()
  display: string = 'none'
  
  constructor() { }

  ngOnInit(): void {
  }
  ngAfterViewInit(): void{
    getElementById(this.userid, this.display)
  }

}
