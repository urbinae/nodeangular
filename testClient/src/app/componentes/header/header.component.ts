import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../SERVICES/auth.service'

declare const openNav: any;
declare const closeNav: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    openNav()
    closeNav()
  }

  logout(){
    this.authService.logout()
  }

}
