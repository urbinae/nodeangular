import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/SERVICES/auth.service';
import { UserI } from 'src/app/models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  noSuccess: Boolean = false
  message: string = ''

  constructor(private autthservice: AuthService, private router: Router) {
    autthservice.checkToken();
    if (autthservice.noSuccess) {
      this.noSuccess = autthservice.noSuccess
      this.message = autthservice.message
    }
  }

  ngOnInit(): void {
  }

  onLogin(form: { value: any; }): void {
    // console.log('Login', form.value)
    this.autthservice.checkToken();
    this.autthservice.login(form.value).subscribe(
      res => {
        this.router.navigateByUrl('/dashboard')
      }
    )

  }

}
