import { Component, OnInit, Input } from '@angular/core';
import { User, UsersService } from 'src/app/SERVICES/users.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/SERVICES/auth.service';

declare const openTab: any;

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css']
})
export class TabComponent implements OnInit {

  //variable
  ListaUsuarios: User[] = [];

  UsuariosPendientes: User[] = [];
  UsuariosAprobados: User[] = [];
  UsuariosRechazados: User[] = [];

  constructor(private UsersService: UsersService,
    private router: Router,
    private authSevice: AuthService) { }

  ngOnInit(): void {
    // this.listarUsuarios()
    this.listarUsuariosPendientes(0)
    this.listarUsuariosAprobados(1)
    this.listarUsuariosRechazados(2)
  }

  listarUsuarios() {
    this.UsersService.getUsers().subscribe(
      res => {
        this.ListaUsuarios = <any>res
        console.log(res)
      },
      err => console.log(err)
    )
  }

  listarUsuariosPendientes(state: number) {
    this.UsersService.getUsersByState(state).subscribe(
      res => {
        this.UsuariosPendientes = <any>res
      },
      err => console.log(err)
    )
  }

  listarUsuariosAprobados(state: number) {
    this.UsersService.getUsersByState(state).subscribe(
      res => {
        this.UsuariosAprobados = <any>res
      },
      err => console.log(err)
    )
  }

  listarUsuariosRechazados(state: number) {
    this.UsersService.getUsersByState(state).subscribe(
      res => {
        this.UsuariosRechazados = <any>res
      },
      err => console.log(err)
    )
  }
  eliminarUsuario(id: number) {
    this.UsersService.deleteUser(id).subscribe(
      res => {
        this.listarUsuariosPendientes(0)
        this.listarUsuariosAprobados(1)
        this.listarUsuariosRechazados(2)
      },
      err => console.log(err)
    )
  }

  actualizarUsuario(id: number) {
    this.router.navigate(['/update/' + id])
  }
}
