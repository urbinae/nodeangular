import { Component, OnInit } from '@angular/core';
import { UsersService, User, CreateUser } from 'src/app/SERVICES/users.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/SERVICES/auth.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  errors: string[] = []
  thereAreErrors: Boolean = false

  user: CreateUser = {
    name: '',
    lastname: '',
    state: 0,
    createon: new Date()
  }
  constructor(private UsersService: UsersService, private router: Router, private authService: AuthService) { 
    authService.checkToken()
  }

  ngOnInit(): void {
  }

  crear() {
    this.errors = []
    this.UsersService.createUser(this.user).subscribe(
      res => {
        console.log(res)
        this.router.navigate(['/dashboard'])
      },
      err => {
        this.thereAreErrors = true
        err.error['errors'].forEach((error: any) => {
          this.errors.push(error['msg'])
          console.log(error['msg'])
        });
        console.log(this.errors)
      }
    )
  }

}
