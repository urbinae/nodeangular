import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/SERVICES/auth.service';
import { User, UsersService } from 'src/app/SERVICES/users.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  errors: string[] = []
  thereAreErrors: Boolean = false

  user: User = {
    id: 0,
    name: '',
    lastname: '',
    state: 0,
    createon: new Date()
  }
  constructor(private UsersService: UsersService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private authService: AuthService) { 
      authService.checkToken()
    }

  ngOnInit(): void {
    const id_entrada = this.activeRoute.snapshot.params['id']
    if (id_entrada) {
      this.UsersService.getUser(id_entrada).subscribe(
        res => {
          this.user = <any>res
        },
        err => console.log(err)
      )
    }
  }

  modificar() {
    this.UsersService.updateUser(this.user.id, this.user).subscribe(
      res => {
        console.log(res)
        this.router.navigate(['/dashboard'])
      },
      err => {
        this.thereAreErrors = true
        err.error['errors'].forEach((error: any) => {
          this.errors.push(error['msg'])
          console.log(error['msg'])
        });
        console.log(this.errors)
      }
    )
  }
}
