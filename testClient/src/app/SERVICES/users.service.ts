import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  url = '/api/users'
  constructor(private http: HttpClient) { }

  //obtener usuarios
  getUsers() {
    return this.http.get(this.url)
  }

  //obtener usuarios
  getUsersByState(state: number) {
    return this.http.get(this.url + '/bystate/' + state)
  }

  //obtener un usuario
  getUser(id: number) {
    return this.http.get(this.url + '/' + id)
  }

  //crear usuario
  createUser(user: CreateUser) {
    return this.http.post(this.url + '/create', user)
  }

  //Eliminar usuario
  deleteUser(id: number) {
    return this.http.delete(this.url + "/" + id)
  }

  //Modificar usuario
  updateUser(id: number, user: User) {
    return this.http.put(this.url + '/update/' + id, user)
  }
}

export interface User {
  id: number
  name?: string
  lastname?: string
  state?: number
  createon: Date
}

export interface CreateUser {
  name?: string
  lastname?: string
  state?: number
  createon: Date
}