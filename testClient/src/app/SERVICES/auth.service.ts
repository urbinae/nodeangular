import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserI } from '../models/user';
import { JwtResponseI } from '../models/jwt-response';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

const helper = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  AUTHSERVER: string = "http://localhost:3000"
  AuthSubject = new BehaviorSubject(false)
  private token: string = '';
  noSuccess: Boolean = false
  message: string = ''

  constructor(private httpClient: HttpClient, private router: Router) {
    // this.checkToken();
  }

  login(user: UserI): Observable<JwtResponseI> {
    return this.httpClient.post<JwtResponseI>(`${this.AUTHSERVER}/api/users/login`,
      user).pipe(tap(
        (res: JwtResponseI) => {
          console.log(res)

          if (res.success) {
            this.saveToken(res.dataUser.accessToken, res.dataUser.expiresIn)
          } else {
            this.noSuccess = true
            this.message = res.message
          }
        }
      ))
  }

  register(user: UserI): Observable<JwtResponseI> {
    return this.httpClient.post<JwtResponseI>(`${this.AUTHSERVER}/api/users/register`,
      user).pipe(tap(
        (res: JwtResponseI) => {
          // console.log(res.success)
          // if (!res.success) {
          //   this.noSuccess = true
          //   this.message = res.message
          // }
        })
      );
  }

  logout(): void {
    this.token = ''
    localStorage.removeItem('ACCESS_TOKEN')
    localStorage.removeItem('EXPIRES_IN')
    this.router.navigate(['/auth/login']);
  }

  private saveToken(token: string, expiresIn: string) {
    localStorage.setItem('ACCESS_TOKEN', token)
    localStorage.setItem('EXPIRES_IN', expiresIn)
    this.token = token
  }

  checkToken(): void {
    this.token = localStorage.getItem('ACCESS_TOKEN') || ''
    // console.log('token: ', this.token)
    if (this.token != '') {
      const isExpired = helper.isTokenExpired(this.token);
      // console.log('expirado: ', isExpired)
      if (isExpired) {
        this.logout();
      }
    }
    else {
      this.logout();
    }
  }
}
